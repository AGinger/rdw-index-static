let fileswatch = 'html,htm,txt,json,md,woff2' // List of files extensions for watching & hard reload

const { src, dest, parallel, series, watch } = require('gulp')
const browserSync  = require('browser-sync').create()
const webpack      = require('webpack-stream')
const sass         = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')
const rename       = require('gulp-rename')
const imagemin     = require('gulp-imagemin')
const webp         = require('gulp-webp')
const cssbeautify  = require('gulp-cssbeautify')
const newer        = require('gulp-newer')
const rsync        = require('gulp-rsync')
const del          = require('del')
const injectVersion = require('gulp-inject-version')
const include = require('gulp-include')

function browsersync() {
	browserSync.init({
		server: { baseDir: 'dist/' },
		notify: false,
		online: true
	})
}

function scripts() {
	return src('app/js/app.js')
	.pipe(webpack({
		mode: 'production',
		module: {
			rules: [
				{
					test: /\.(js)$/,
					exclude: /(node_modules)/,
					loader: 'babel-loader',
					query: {
						presets: ['@babel/env']
					}
				}
			]
		}
	})).on('error', function handleError() {
		this.emit('end')
	})
	.pipe(rename('app.min.js'))
	.pipe(dest('dist/js'))
	.pipe(browserSync.stream())
}

function styles() {
	return src('app/scss/main.scss')
	.pipe(sass())
	.pipe(autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true }))
	.pipe(cssbeautify({
		indent: '  '
	}))
	.pipe(rename('app.css'))
	.pipe(dest('dist/css'))
	.pipe(browserSync.stream())
}

function images() {
	return src('app/img/**/*')
	// .pipe(imagemin())
	.pipe(dest('dist/img'))
}

function fonts(){
  return src('app/fonts/*')
  .pipe(dest('dist/fonts'))
}

function icons(){
  return src('app/icons/*')
  .pipe(dest('dist/icons'))
}

function webpimg() {
  return src('app/img/_towebp/*.{jpg,jpeg,png,gif,ico}')
	.pipe(webp({
		quality: 80
	}))
	.pipe(dest('app/img/_webp'))
}

function html() {
	return src('app/**/*.html')
	.pipe(include()).on('error', console.log)
	.pipe(injectVersion())
	.pipe(dest('dist/'))
	.pipe(browserSync.stream())
}

function version() {
	return src('app/*.html')
	.pipe(injectVersion())
	.pipe(dest('dist/'));
}

function cleanimg() {
	return del('app/img/dest/**/*', { force: true })
}

function deploy() {
	return src('app/')
	.pipe(rsync({
		root: 'app/',
		hostname: 'username@yousite.com',
		destination: 'yousite/public_html/',
		include: [/* '*.htaccess' */], // Included files to deploy,
		exclude: [ '**/Thumbs.db', '**/*.DS_Store' ],
		recursive: true,
		archive: true,
		silent: false,
		compress: true
	}))
}

function startwatch() {
	watch('app/**/*.html', { usePolling: true }, html)
	watch('app/fonts/*', { usePolling: true }, fonts)
	watch('app/icons/*', { usePolling: true }, icons)
	watch('app/scss/**/*', { usePolling: true }, styles)
	watch(['app/js/**/*.js', '!app/js/**/*.min.js'], { usePolling: true }, scripts)
	watch('app/img/**/*.{jpg,jpeg,png,webp,svg,gif}', { usePolling: true }, images)
	watch(`app/**/*.{${fileswatch}}`, { usePolling: true }).on('change', browserSync.reload)
}

exports.assets   = series(cleanimg, scripts, images)
exports.scripts  = scripts
exports.styles   = styles
exports.images   = images
exports.icons    = icons
exports.fonts    = fonts
exports.webpimg  = webpimg
exports.html     = html
exports.version  = version
exports.cleanimg = cleanimg
exports.deploy   = deploy
exports.default  = series(html, scripts, images, styles, fonts, icons, parallel(browsersync, startwatch))
