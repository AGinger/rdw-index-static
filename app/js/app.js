require('./libs/modernizr-custom.js'); // adds lib to detect browser webp support
require('./libs/custom-select.min.js'); // adds lib to detect browser webp support
// Dependancies
require('./project/header-customizer.js'); // Swiper Slider Settings
require('./project/swiper-customizer.js'); // Swiper Slider Settings
require('./project/webp-customizer.js'); // set fallback for webp images
require('./project/sidebar.js'); // sidebar controls
require('./project/search-block.js'); // search header
require('./project/tabs.js'); // tabs
require('./project/menu-drop.js'); // dropdown side desktop menu
require('./project/menu-desktop.js'); // header dropdown desktop menu
require('./project/custom-select-customizer.js'); // form file button customization
require('./project/gallery.thumb.js'); // thumbnail gallery