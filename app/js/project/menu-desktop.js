document.addEventListener('DOMContentLoaded', function() {
  (function(){

    let menuBtn = document.querySelectorAll('.header-droplink');

    menuBtn.forEach( (btn) => {
      btn.addEventListener('mouseover', (e) => {
        btn.classList.add('active');
        e.stopPropagation();
      });
    });

    // close on document click
    document.addEventListener('click', function(){
      menuBtn.forEach( (btn) => {
        btn.classList.remove('active')
      });
    }, false);

    // close on scroll
    document.addEventListener('scroll', function(){
      menuBtn.forEach( (btn) => {
        btn.classList.remove('active')
      });
    }, false);

  })();
});
