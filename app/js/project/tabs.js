document.addEventListener('DOMContentLoaded', function() {
  (function(){

    let button = document.querySelectorAll('.tab-head-desk-btn'),
        tab = document.querySelectorAll('.tab-content-item'),
        i;

    // desktop tabs
    button.forEach( (btn) => {
      btn.addEventListener('click', () => {

        let tabId = btn.getAttribute('data-tab');

        for( i = 0; i < button.length; i++ ) {
          button[i].classList.remove('active');
        }

        for( i = 0; i < tab.length; i++ ) {
          tab[i].style.display = 'none';
        }

        btn.classList.add('active');
        document.getElementById('tab' + tabId).style.display = 'block';

      });
    });

    // sync custom select whith tabs for mobile
    let customSelect = document.getElementById('customSelect');
    if (customSelect) {
      customSelect.addEventListener('change', function(event) {
        let optionId = event.target.value;
        console.log(optionId)
        for( i = 0; i < tab.length; i++ ) {
          tab[i].style.display = 'none';
        }
        document.getElementById('tab' + optionId).style.display = 'block';
      });
    }

  })();

});