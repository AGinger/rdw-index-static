document.addEventListener('DOMContentLoaded', function() {

  (function () {

    let header = document.querySelector('header');

    window.addEventListener("scroll", (event) => {
      let scroll = this.scrollY;
      if (scroll !== 0) {
        header.classList.add('header-compact')
      } else {
        header.classList.remove('header-compact')
      }
    });

  })();

});