// if webp is supported add class to body tag
Modernizr.on('webp', function(result) {
  let body = document.getElementById('body-wrap');
  if (result) {
    //console.log('webp is supported')
    body.classList.add('webp');
    body.classList.remove('no-webp');
  } else {
    // not-supported
    //console.log('webp isn\'t supported');
    body.classList.remove('webp');
    body.classList.add('no-webp');
  }
});

// safari fix for replacing inline background-image form webp to jpg
document.addEventListener('DOMContentLoaded', function() {

  let isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

  if (isSafari) {
    let fixElement = document.querySelectorAll('.safari-fix'), i;
    for (i = 0; i < fixElement.length; ++i) {
      let format = fixElement[i].dataset.format,
          style = fixElement[i].style.backgroundImage;
          style = style.replace('.webp', '.' + format);
      fixElement[i].style.backgroundImage = style;
    }
  }
  
});
