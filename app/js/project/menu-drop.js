document.addEventListener('DOMContentLoaded', function() {
  (function(){

    let menuBtn = document.querySelectorAll('.menu-dropdown-icon');

    menuBtn.forEach( (btn) => {
      btn.addEventListener('click', () => {
        let drop = btn.parentElement;
        drop.classList.toggle('drop');
      });
    });

  })();
});