document.addEventListener('DOMContentLoaded', function() {
  (function(){
    let navIconOpen = document.getElementById('nav-icon-open'),
        navIconClose = document.getElementById('nav-icon-close'),
        page = document.querySelector('.page'),
        expandSection = document.querySelectorAll('.nav-expand'),
        sidebar = document.getElementById('sidebar');

    navIconOpen.addEventListener('click', function (e) {
      sidebar.classList.add('active');
      page.classList.add('fade');
      e.stopPropagation();
    });

    navIconClose.addEventListener('click', function () {
      sidebar.classList.remove('active');
      page.classList.remove('fade');
      for (let i = 0; i < expandSection.length; i++) {
        expandSection[i].classList.remove('active');
      }
    });

    // close on document click
    document.addEventListener("click", function(){
      sidebar.classList.remove('active')
      page.classList.remove('fade');
    }, false);

    console.clear()

    const navExpand = [].slice.call(document.querySelectorAll('.nav-expand'))
    const backLink = `<li class="nav-item">
      <a class="nav-link nav-back-link" href="javascript:;">
        Обратно
      </a>
    </li>`

    navExpand.forEach(item => {
      item.querySelector('.nav-expand-content').insertAdjacentHTML('afterbegin', backLink)
      item.querySelector('.nav-link').addEventListener('click', function (e) {
        item.classList.add('active')
        e.stopPropagation()
      })
      item.querySelector('.nav-back-link').addEventListener('click', function (e) {
        item.classList.remove('active')
        e.stopPropagation()
      })
    })

  })();
});
