# README #

Файл с описанием файловой структуры вёрстки фронта. На него есть ссылка из документации проекта. Информация не дублируется там.

## Описание версий ## 


### 1.9 SE ###

Special Edition специально для демонстрации генеральным 15-го сентября. Скрыты табы на которые нет контента. Также присутствуют нужные доделки по контенту.

### 1.8.6 ###

Последняя версия с косяком отсутствия ссылок с карточек товаров. С карточек линеек ссылки есть.

## Файловая структура ##

* Шаблоны для бэка лежат там *app/dist*.
* Готовая вёрстка */dist*.
* Шаблоны отдельных позиций находятся в */production-items*.
* Шрифты используем из Google Fonts, но подгружаем их локально из */fonts* соответственно.
* Иллюстрации в */img*.
* Инклюды (части) шаблонов в */html-parts*.

♪